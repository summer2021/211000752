# ref doc: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

# deploy the Dashboard UI
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml

# access the Dashboard UI
nohup kubectl proxy --accept-hosts='^.*' &

# use SSH local port forward to expose service outside
# access http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
ssh -L 8001:localhost:8001 root@139.224.236.157