###
# This script is used for initializing the configuration for a kubernetes cluster
# ./k8s_config.sh [hostname]
###

# disable swap
sed -i '/swap/d' /etc/fstab
sudo swapoff -a

# change hostname 
hostnamectl set-hostname $1

# enable network
# sed -i '/^ONBOOT/s/no/yes/' /etc/sysconfig/network-scripts/ifcfg-enp0s3
sed -i '/^ONBOOT/s/no/yes/' /etc/sysconfig/network-scripts/ifcfg-enp0s8
service network restart

yum install net-tools -y

# let ip_table see bridged network 
sudo modprobe br_netfilter
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

# diable firewall
systemctl stop firewalld
systemctl disable firewalld

# install docker
sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start  docker

# config cgroup driver
mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "registry-mirrors":["https://b9pmyelo.mirror.aliyuncs.com"]
}
EOF

# config docker proxy
mkdir /etc/systemd/system/docker.service.d && \
cat <<EOF | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://1.117.225.82:10808"
Environment="HTTPS_PROXY=http://1.117.225.82:10808"
Environment="NO_PROXY=localhost,127.0.0.0/8"
EOF

sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker

# install k8s components
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubelet kubeadm kubectl
systemctl enable kubelet && systemctl start kubelet



