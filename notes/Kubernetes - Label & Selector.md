Tags: #Concept

## Definition
Labels are a grouping primitives that allow logical operation on objects in K8s.  

## Application

Labels are key-value pairs attached to objects and can be used in any number of ways:
- Designate objects for development, test and production
- Embed version tags
- Classify an object using tags
![](https://d33wubrfki0l68.cloudfront.net/7a13fe12acc9ea0728460c482c67e0eb31ff5303/2c8a7/docs/tutorials/kubernetes-basics/public/images/module_04_labels.svg)
