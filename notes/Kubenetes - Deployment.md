Tags: #Concept

## Definition
Once you have a running Kubernetes cluster, you can deploy your containerized applications on top of it. To do so, you create a K8s Deployment configuration. Once you have created a Deployment, the K8s Control Plane schedules the application instances included in that Deployment to run on individual Nodes in the cluster. 

##  Property

The Deployment Controller provides a self-healing mechanism to address machine failure or maintenance. Scaling is accomplished by changing the number of replicas in a Deployment. K8s also support [auto-scaling](https://kubernetes.io/docs/user-guide/horizontal-pod-autoscaling/) of [[Kubernetes - Pod]]. 

Running multiple instances of an application will require a way to distribute the traffic to all of them. [[Kubernetes - Service]] have an integrated load-balancer that will distribute network traffic to all Pods of an exposed Deployment. 