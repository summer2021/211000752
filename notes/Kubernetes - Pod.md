Tags: #Concept

## Definition
### What's a Pod

> A Pod is a Kubernetes abstraction that represents a group of one or more application containers (such as Docker), and some shared resources for those containers.

A Pod is an application-specific "**logical host**" and can contain different application containers which are tightly coupled. Eg: a Pod might include both the container with your Node.js app as well as a different container that feeds the data to be published by the Node.js web server.

**Pods are the atomic unit of K8s**. The containers in a Pod share an IP Address and port space are always co-located and co-scheduled, and run in a shared context on the same Node.

### Why we need Pod
A Container is designed to run only a single process. When we need a complicate application which need multiple correlated processes share resources, then we shouldn't organize them in a single container but among multiple related containers which is a Pod in K8s.

##  Property

Pods share resources include:
- storage: volume
- network: cluster IP address
- information about how to run each container: the container image version or specific ports to use


### Pods and Nodes
A Pod always runs on a Node. A Node is a worker machine in K8s and may be either a virtual or a physical one. 

A Node can have multiple Pods, and the K8s Control Panel automatically handles scheduling the pods across the Nodes in the cluster.

Kubernetes Pods are mortal and have lifecycle.  For example, a worker node dies, the Pods running on that node are also lost. A ReplicaSet might then dynamically drive the cluster back to desired state via creation of new Pods to keep your application running. Each Pod in a K8s cluster has unique IP even Pods on the same Node.