When you create an [[Kubernetes Object]] in Kubernetes, you must provide the object spec that describe its desired state, as well as some basic information about the object. 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:   # pod template
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80

```


Required Field:
- object metadata
	- `apiVersion`: which version of the Kubernetes API you are using to create this object
	- `kind`: which kind of object you want to create
	- `metadata`: data help uniquely identify the object, including a name string, UID, and an optional `namespace`
- `spec`: what state you desire for the object
- `status`: current state of the object