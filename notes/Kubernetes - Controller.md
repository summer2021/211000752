Tags: #Concept

## Definition

### What's a Controller

A [Controller](https://kubernetes.io/docs/concepts/architecture/controller/) tracks at least one Kubernetes resource type (or Kubernetes Objects) which have a spec field that represents the desired state. The Controllers for that resource are responsible for making the current state come closer to that desired state.

The Controller might carry the action out itself (Direct Control, eg: Node Controller) or send messages to the API Server that have useful side effects (Control through API Server, eg: Job Controller).

### Controllers in Kubernetes

As a tenet of design, Kubernetes uses lots of Controllers that each manage a particular aspect of cluster state. Most commonly, a particular Controller uses one kind of resources as its desired state and has a different kind of resource that it manages to make that desired state happen. For example, A Job Controller tracks job objects(to find out its state) and Pod objects (to run the jobs and then to see when the work is finished).

With different controllers for different resources, K8s is able to handle constant changes in the cluster automatically. Potentially, the cluster could never reach the desired stable state. However, as long as the controllers are running and able to make useful changes, it doesn't matter for K8s if the overall state is stable or not.

### Controller Implementation
It's useful to have simple controllers rather than one, monolithic set of control loops that are interlinked. Controllers can fail and K8s is designed to allow for that.

Kubernetes comes with a set of built-in controllers that run inside the Kube-Controller-Manager, which provide important core behaviors. For example, the Deployment Controller and Job Controller are examples of controllers that come as part of Kubernetes itself.

You can also have controllers that run outside the Control Plane to extend K8s. You can run your own Controllers as a set of Pods, or externally to K8s.