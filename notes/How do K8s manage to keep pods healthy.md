Tags: #Point

## What's the problem
How do K8s manage to keep pods healthy

## How to fix it
- How to tell if a pod is in a good state
	- Through whether the process in this pod is running or crashes. Not appropriate for conditions when processes keep running when they are not in a good state. Eg1: JVM throws OOM, they will keep running,  Eg2: Process in a infinite loop
	- The problem is K8s cannot make the assumption that the unhealthy process will properly exit, you must check the state from the outside rather than depending on the app doing it internally. 
	- K8s introduces [[Kubernetes - Liveness Probe]] to check if a Pod is alive. You can specify a liveness probe for each container in the pod's specification, which is basically the conditions used to check if it is alive.  If no Liveness Probe is specified for a container, then K8s will only get the state from whether the process is running.





