Tags: #Concept

## Definition
 A liveness probe for each container in the Pod's specification is basically the conditions Kubernetes used to check if it is alive. 
 
 This Liveness Probe check is performed by Kubelet on the node hosting the pod. Therefore if the node itself crashes, this mechanism can't do anything but wait for the Control Plane components running on the master to restart the node based on other mechanisms such as [[Kubernetes - Replication Controller]].

##  Property
The conditions can be one of the following three:
		- HTTP GET: perform an HTTP GET request on the specified port and path and check the response
		- TCP Socket: check if can open an TCP connection to the specified port
		- Exec: executes an arbitrary command inside the container and check the commands' exit status code.

## Application
### miscellaneous
- common Pods' exit status code:
	- 137: 128+9 (SIGKILL), killed outside forcibly by the signal
	- 143: 128+15 (SIGTERM)
- additional properties for Liveness Probe
	- `initialDelaySeconds`: Kubernetes will wait for some seconds to make the initial attempt. Important to set because or else the app isn't ready to start to receive requests.
	- `timeout`: HTTP GET timeout
	- `period`: every n seconds to make an attempt

### rules for designing a Liveness Probe
1. For the most of time, the simplistic Liveness Probe (just check if the server is responding) is sufficient. For a better check, you can have the app perform an internal status check for all the vital components on the Liveness Probe API. **But be sure to only check internals of the app and nothing influenced by external factors**. For example, a frontend web server should not return a failure check when the DB connection is lost. Because you cannot rescue that by restarting this container.
2. Keeping the probes light. The probes are executed relatively often and are by default only allowed one second to complete. For example, be sure to use HTTP probe rather that Exec probe, because the latter will spin up a whole new JVM to get the liveness information.
3. Don't implementing the retry loops in your Probes. Because this is already  considered by the K8s.


