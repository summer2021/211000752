Tags: #Collection 

## Architecture
![[Kubenetes Architecture.png]]

## Basic Concepts

### Pod
![[Kubernetes - Pod]]

### Controller
![[Kubernetes - Controller]]

### Service
![[Kubernetes - Service]]

### Deployment
![[Kubenetes - Deployment]]

## Design
### Object

![[Kubernetes Object]]


## Useful Command
- kubectl cluster-info: check cluster status
- kubectl get
	- kubectl get node
	- kubectl get pods: look for existing Pods
	- kubectl get services: look for existing Services 
- kubectl expose:  create new service 
- kubectl describe: show details
- kubectl create deployment
- kubectl label
- kubectl exec -ti $POD_NAME -- curl localhost:8080
- kubectl delete svc 
- kubectl logs (--previous)
- kubectl rollout status 
- kubectl set image
- kubeadm reset

- [delete pods and node](https://www.bluematador.com/blog/safely-removing-pods-from-a-kubernetes-node)

