Tags: #Concept 

## Definition

Kubernetes use [Objects](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/) to represent the state of the cluster. Specifically, Objects can describe: 
- What containerized applications are running (and on which nodes) - [[Kubernetes - Pod]]
- The resources available to those applications - Node
- The polices around how those applications behave, such as restart policies, upgrades, and fault-tolerance - [[Kubenetes - Deployment]]

A Kubernetes Object is a "record of intent". By creating an Object with a [[Kubernetes Specification file]], you are effectively telling the Kubernetes what you want your cluster's workload to look like, which is your cluster's desired state. Once you create the Object, [[Kubernetes - Controller]] will constantly work to ensure that object exists and achieves the desired state.

Also, Kubernetes provides several ways to organize these Objects.  ![[Kubernetes - Namespace#^2caafd]]


## Examples
- [[Kubernetes - Job]]: running pod that performs a single completable task
- [[Kubernetes - CronJob]]: schedule a job to run periodically or once in the future
- [[Kubernetes - Replication Controller]]: keep the desired number of pod replicas
running
- [[Kubenetes - Replication Set]]: more powerful Replication Controller with finer control over label selector control