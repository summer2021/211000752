Tags: #Concept 

## Definition
K8s use [[Kubernetes - Label & Selector]] and Namespace to organize Kubernetes objects like Pods. The difference is that each object can have multiple Labels but only one Namespace, which means that the groups organized by Label can overlap while Namespace based groups are separate and non-overlapping. ^2caafd

##  Property
Namespaces doesn't guarantee isolation. For example, pods from different Namespaces can communicate with each other if they know the IP address. And the network isolation depends on which network solution is deployed in Kubernetes.

Also, Nodes are shared in different Namespaces among a Cluster.






