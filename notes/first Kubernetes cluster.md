# install 
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

## disable swap

```bash
# disable swap temporarily
sudo swapoff -a 

# disable permanently
sed -i '/swap/d' /etc/fstab
```


## centos initial config
安装centos7后启动网络连接
```bash
sed -i '/^ONBOOT/s/no/yes/' /etc/sysconfig/network-scripts/ifcfg-enp0s3
# modify `ONBOOT = yes`
service network restart
```

VirtualBox port forwarding settings
https://medium.com/platform-engineer/port-forwarding-for-ssh-http-on-virtualbox-459277a888be

(Optional )yum添加rpm来源， 安装ifconfig
```bash
sudo yum install epel-release
yum install net-tools
```


## host uniqueness check
检查mac, product_uuid, hostname
```bash
ip link
sudo cat /sys/class/dmi/id/product_uuid
sudo hostnamectl set-hostname NEW_HOSTNAME
```


## network 
let ip_table see bridged traffic
```bash
lsmod | grep br_netfilter
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

```

[firewall open port/ disable firewall](https://www.huaweicloud.com/articles/215b0cbccde290e8e820debc2bf3a397.html)
```bash
# open port for firewall
firewall-cmd --permanent --zone=<zone> --add-port=80/tcp

# reload the firewall for changes to take effect.
firewall-cmd --reload

# view open ports
firewall-cmd --list-ports


---

# stop firewall
systemctl stop firewalld

# diable firewall
systemctl disable firewalld
```


## container runtime
install runtime([docker](https://docs.docker.com/engine/install/centos/))
```bash
# 1. install yum-utils
sudo yum install -y yum-utils

# 2. add docker repo to yum with yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# 3. install docker run time
sudo yum install -y docker-ce docker-ce-cli containerd.io

# 4. start docker
sudo systemctl start  docker

```

## k8s components
install kubeadm, kubelet, kubectl（[国内环境](https://www.orchome.com/10036)）
```bash
# 配置国内镜像
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF


# 安装
setenforce 0
yum install -y kubelet kubeadm kubectl

# 启动
systemctl enable kubelet && systemctl start kubelet
```


## cgroup driver 
configure cgroup driver (match container runtime and kubelet)
```bash
mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "registry-mirrors":["https://b9pmyelo.mirror.aliyuncs.com"]
}
EOF

sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker
```

# create cluster
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm
https://www.codeleading.com/article/48875765861/
https://zhuanlan.zhihu.com/p/182429596


## setup GFW proxy
Set up a V2ray Docker container running on the host machine at 10808 firstly.
Then set host:10808 as proxy for each VM.

```bash
mkdir /etc/systemd/system/docker.service.d
cat <<EOF | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://1.117.225.82:10808"
Environment="HTTPS_PROXY=http://1.117.225.82:10808"
Environment="NO_PROXY=localhost,127.0.0.0/8"
EOF

sudo systemctl daemon-reload
sudo systemctl show --property Environment docker
sudo systemctl restart docker
```


## start K8s Master
```bash
kubeadm init \
--apiserver-advertise-address=139.224.236.157 \
--pod-network-cidr=10.244.0.0/16    \
--kubernetes-version=v1.18.8 
```

## config K8s master
```bash
# as a regular user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# as root 
export KUBECONFIG=/etc/kubernetes/admin.conf
```


## setup K8s network addon
```bash
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f kube-flannel.yml
```


## node join cluster
```bash
kubeadm join 192.168.56.101:6443 --token itfqi8.pl2xwdrapbqt4nkg \
        --discovery-token-ca-cert-hash sha256:bdf9798ea28d066f4f966f0a8137b60139b11447670952087d2c1ba31d09bfb9
```



# Problems meet

## /proc/sys/net/ipv4/ip_forward contents are not set to 1

**Source**: `kubeadm reset` cannot clean network settings
**Solutions**: We need to set this manually with:
```bash
echo 1 > /proc/sys/net/ipv4/ip_forward
```

## K8s Node state not ready `cni config not initialized`
https://github.com/kubernetes/kubernetes/issues/48798
https://www.huaweicloud.com/articles/57fb977785d386a315bc6319fefdec3b.html
https://stackoverflow.com/questions/49112336/container-runtime-network-not-ready-cni-config-uninitialized

**Source**: 因为kubelet配置了network-plugin=cni，但是还没安装，所以状态会是NotReady
**Solutions**: 不想看这个报错或者不需要网络，就可以修改kubelet配置文件，去掉network-plugin=cni 就可以了。

Update: 在使用`kubectl apply`安装网络插件后，插件相关的Pod会自动部署到各个节点，但是由于前期部署集群到虚拟机时，master与node之间网络不通，所以导致flannel插件不能安装到node上，于是node一直处于NotReady状态。

## VM NAT-Network fail after cluster started
**Source**: `flannel` addon modify the iptable rules which are used by NAT-Network.
**Solutions**: assign two network card to each VM, one for communicating with each other within the cluster, one for connecting to outside Internet.

## Kernel Stuck reported by syslogd

```
Message from syslogd@master at Jul 22 13:51:14 ...
 kernel:NMI watchdog: BUG: soft lockup - CPU#0 stuck for 32s! [etcd:21818]
```

Source: not known


## Servant Job(yurtctl-servant-convert-node1) timeout

```
E0827 13:47:18.603565   24516 util.go:476] fail to run servant job(yurtctl-servant-convert-node1): wait for job to be complete timeout
```

**Source**:  
```
[root@master1 openyurt]# kubectl logs  yurtctl-servant-convert-node1-b2qt6 -n kube-system
F0827 13:46:42.595019   26279 edgenode.go:83] fail to covert the kubernetes node to a yurt node: stat /etc/systemd/system/kubelet.service.d/10-kubeadm.conf: no such file or directory
```
`10-kubeadm.conf` not found in the default position `/etc/systemd/system/kubelet.service.d`, but in `/usr/lib/systemd/system/kubelet.service.d/`. [This change](https://github.com/kubernetes/kubeadm/issues/1575) was introduced for Centos rpm starting from kubeadm 1.13.5.

**Solutions**: add extra parameter for yurtctl `--kubeadm-conf-path /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf`


## Node autonomy test fail
**Source**:  sudo sed -i 's|--server-addr=.*|--server-addr=https://1.1.1.1:1111|' /etc/kubernetes/manifests/yurt-hub.yaml
修改后，yurt-hub仍然使用正确的server-addr重新创建pod. 原因是我在修改之前在当前文件夹下做了一次备份`cp yurt-hub.yaml yurt-hub.yaml.copy`. 由于这个目录是[kubeadm 设置的 static pod目录](https://kubernetes.io/docs/reference/setup-tools/kubeadm/implementation-details/#constants-and-well-known-values-and-paths)，目录中的每一份文件都会被当作static pod manifest处理，所以，即使检测到了`yurt-hub.yaml`修改，那份copy文件仍然影响到了server-addr的设定

**Solutions**: 移除copy文件


## pod stuck in pending while intalling yurt-app-manager
**Source**: [schedule failed](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-application/#my-pod-stays-pending)
```yaml
Events:
  Type     Reason            Age                    From               Message
  ----     ------            ----                   ----               -------
  Warning  FailedScheduling  7m46s (x76 over 118m)  default-scheduler  0/2 nodes are available: 1 node(s) didn't match node selector, 1 node(s) had taint {node-role.kubernetes.io/master: }, that the pod didn't tolerate.
```

**Solutions**: untaint master `kubectl taint node master1 node-role.kubernetes.io/master:NoSchedule-`
