Tags: #Book #todo 
---
Title: Kubernetes In Action
Author: Marko Luksa
Rate: 10
---

## Description
Not only a guide book to get novices' hands on Kubernetes, but explain the basic ideas and internal logic of the whole mechanism behind the scene.

## Structure

4. Replication and other controllers: deploying managed pods
	1. [[How do K8s manage to keep pods healthy]], and why we need [[Kubernetes - Liveness Probe]] to check the liveness state of a container.
	2. 

## Notes

