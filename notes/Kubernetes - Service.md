Tags: #Concept

## Definition
> A Service in K8s is an abstraction which defines a logical set of Pods and a policy by which to access them. 

Service is a way to expose the application running on a set of Pods as a fixed network service.  A Service route traffic across a set of Pods. This way, clients of this Service don't need to care about the location of the individual pods behind the Service.

Discovery and routing among dependent Pods (such as frontend and backend components in an application) is handled by K8s Service.

Services match a set of Pods using [[Kubernetes - Label & Selector]]

##  Property

- defined use YAML(preferred) or JSON  (like all K8s objects)
- Pods organized by LabelSelector
- expose Pods outside (or else they can't be accessed outside) in different ways
	- ClusterIP (default), internal IP in the cluster
	- NodePort, the same port of each selected Node in the cluster, use the node IP
	- LoadBalancer, external IP with a load balancer
	- ExternalName, external host name