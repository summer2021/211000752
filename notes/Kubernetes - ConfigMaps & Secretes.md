Tags: #Concept

## Definition
There are several ways to set environment variables for a Docker container in Kubernetes, including: Dockerfile, kubernetes.yml, Kubernetes ConfigMaps, and Kubernetes Secrets. One of the benefits for using ConfigMaps and Secrets is that they can be **re-used across multiple containers**, including being assigned to different environment variables for the different containers.

ConfigMaps are [[Kubernetes Object]] that store non-confidential key-value pairs. ConfigMaps allow you to decouple configuration artifacts from image content to keep containerized applications portable.

Secrets are also used to store key-value pairs, they differ from ConfigMaps in that they're intended for confidential/sensitive information and are stored using Base64 encoding. This makes secrets the appropriate choice for storing such things as credentials, keys, and tokens. 

##  Property

## Application


