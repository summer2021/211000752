# OpenYurt: 云原生边缘计算平台的易用性提升（211000752）

## 项目成员

- 陈璐 @chenlu_211000752
- 唐炳昌 @tangbingchang_211000752

## 项目背景

OpenYurt 以"Extending your native kubernetes to edge"的设计理念，期待在边缘计算场景探索更多的 Kubernetes 云原生应用可能性。边缘云原生作为边缘计算与云原生的交叉学科，许多来自边缘计算领域的开发者并不熟悉云原生相关的知识。当前，社区已经实现了原生 Kubernetes 与 OpenYurt 集群相互转换的工具，同时也支持了特定节点的转换。然而转换工具仍有一定的学习成本，且操作相对复杂。社区中也提出了 OpenYurt 集群创建及节点接入的 Proposal，但是也未能覆盖所有场景。

本项目主要解决新用户在接触 OpenYurt 时，如何给用户更好的使用体验。通过参与本项目，您可以深入理解 OpenYurt 的架构设计，整体安装流程，从而了解到云原生边缘计算领域的前沿技术。

## 项目结构

```
|- backend              // golang实现后端
    |-- k8s_client      // 后端访问K8s APIServer Client实现
    |-- proxy_server    // 后端接口服务实现
|- doc                  // 项目设计与实现文档，中期报告，结题报告
|- frontend             // React实现前端
    |-- public          // 前端使用外部资源
    |-- src             // React组件代码
|- notes                // k8s & OpenYurt 学习报告
|- scripts              // 集群搭建脚本
```
