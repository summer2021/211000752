# OpenYurt: 云原生边缘计算平台的易用性提升 - 项目报告

## 项目信息

### 项目名称

OpenYurt: 云原生边缘计算平台的易用性提升

### 项目背景

OpenYurt以"Extending your native kubernetes to edge"的设计理念，期待在边缘计算场景探索更多的Kubernetes云原生应用可能性。边缘云原生作为边缘计算与云原生的交叉学科，许多来自边缘计算领域的开发者并不熟悉云原生相关的知识。当前，社区已经实现了原生Kubernetes与OpenYurt集群相互转换的工具，同时也支持了特定节点的转换。然而转换工具仍有一定的学习成本，且操作相对复杂。社区中也提出了OpenYurt集群创建及节点接入的Proposal，但是也未能覆盖所有场景。

### 项目目标

本项目预期完成如下目标，最终交付相应的设计文档和实施代码：

1. 协助社区成员[@gnunu](https://github.com/gnunu) 设计并实现[YurtCluster operator](https://github.com/openyurtio/openyurt/blob/master/docs/proposals/20210722-yurtcluster-operator.md), 用于OpenYurt集群和Kubernetes集群的双向转换
2. 与OpenYurt多租项目的同学合作，设计并实现一个OpenYurt体验平台，为新用户提供一个可快速上手的开箱即用的OpenYurt集群，并且以web console的形式与其交互。我负责的是该系统的前端部分以及集群部署任务。

### 项目规划

项目规划针对上述项目目标，可分解为两部分：

1. YurtCluster operator
   1. 准备阶段，学习Kubernetes及OpenYurt基础知识，本地搭建一个简单集群作为开发环境 [√]
   2. 设计阶段，主要内容包括Operator使用场景的分析，功能边界的确定，Operator中CRD的设计 [√]
   3. 实现阶段
      1. 技术方案调研与学习，确定使用`kubebuilder`进行开发  [√]
      2. 代码实现，主要是Controller部分的实现 [ing]
      3. 测试，测试Operator在不同架构，不同版本集群的兼容情况
2. OpenYurt体验平台
   1. 设计阶段，确定系统的使用场景，需求分析，设计软件的功能和实现的算法和方法、系统的总体结构设计 [√]
   2. 实现阶段（只列举我负责的任务）
      1. 环境搭建，为体验平台搭建配置OpenYurt Master
      2. 前端实现，体验平台的界面设计与实现 [ing]
      3. 前后端联调



## 项目进度

### 已完成工作

- 关于YurtCluster项目，我们已经完成了准备阶段和设计阶段进入到实现阶段。准备阶段中的[学习笔记](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/tree/master/notes)和[开发环境配置脚本文件](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/tree/master/scritps)已经上传到gitlab项目repo中，设计阶段的[设计文档](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/blob/master/doc/20210722-yurtcluster-operator.md)也已经提交上传。实现阶段还在进行中。
- 关于OpenYurt体验平台项目，我们已经完成了设计阶段，[设计文档](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/blob/master/doc)已经上传。实现阶段我需要与多租项目团队的同学配合完成，现在前端部分已经实现了一个简单的[界面原型demo](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/blob/master/yurt_console)，使用React技术栈，包括`React.js`, `React Router`, `Ant Design`等.

具体已完成工作可参照上述项目规划每项后的标注。

### 遇到的问题和解决方案

在参与这个项目的过程中，我主要遇到了两方面的问题。

- 首先谈谈非技术方面的问题。由于这个项目remote的性质，缺少直接与导师面对面沟通的机会。当然这不只是我一个人的问题，但是对于缺乏k8s背景知识的我来说仍然是一个不小的考验。特别是在准备阶段，很多场景相关的问题很难用文字描述清楚，只能先自行摸索试验，之后整理出来再向导师求证。但是好在导师和社区成员不厌其烦帮我这个初学者解答一些silly questions。另外，OpenYurt社区的老师们还会每周定期抽时间和我们sync，大家一起平等地参与到讨论中，关于不同的设计方案，不同的实现途径。这个讨论的过程也让我开阔了眼界，学习到了很多云原生开发的相关知识。
- 在这个项目里，我也遇到了很多技术问题，当然这与我在云原生开发方面薄弱的知识储备有关。我将遇到的技术问题整理到了一份[学习文档](https://gitlab.summer-ospp.ac.cn/summer2021/211000752/-/blob/master/doc/first%20Kubernetes%20cluster.md#problems-meet)中，已经上传到gitlab上。

### 后续工作安排

- 关于YurtCluster项目，之后我将配合社区成员[@gnunu](https://github.com/gnunu) 一起完成实现阶段，现在我们已经确定使用[kubebuilder](https://book.kubebuilder.io/)这个Operator框架来完成代码实现，届时实现代码将上传到gitlab中，并且以PR形式提交到[OpenYurt](https://github.com/openyurtio/openyurt)项目中。
- 关于OpenYurt体验平台项目，我还需要将demo扩展完善为一个完整的前端项目，并且与多租团队的同学合作完成前后端联调的工作。除此之外，我还需要为体验平台配置搭建OpenYurt Master供开发使用。





